#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#include "bag.h"

//Déclaration d'une sémaphore
sem_t semaphoreAccesUnique;
sem_t semaphoreTailleRestante;
sem_t semaphoreTailleUsed;

/*
    int sem_init(sem_t *sem, int pshared, unsigned int value);

    sem points to a semaphore object to initialize
    pshared is a flag indicating whether or not the semaphore should be shared with fork()ed processes. LinuxThreads does not currently support shared semaphores
    value is an initial value to set the semaphore to
 */

//     sem_post - unlock a semaphore (REALTIME) 

//     sem_wait, sem_trywait - lock a semaphore (REALTIME) 

bag_t * bb_create(int size) {
    assert(size > 0);
    //Init du sémaphore
    //Adresse du sémaphore, héritage de la sémaphore pour les fork, nb de jetons initaux
    sem_init(&semaphoreAccesUnique, 0, 1);
    sem_init(&semaphoreTailleRestante, 0, size);
    sem_init(&semaphoreTailleUsed, 0, 0);

    bag_t *bag = malloc(sizeof (bag_t));
    assert(bag != NULL);

    bag->elem = malloc(size * sizeof (void*));
    assert(bag->elem);

    bag->size = size;
    bag->count = 0;
    bag->is_closed = 0;

    return bag;
}

void bb_add(bag_t * bag, void * element) {
    assert(bag != NULL); // sanity check

    //while( bag->count >= bag->size ) { } // CAUTION: this synchronization is bogus
    //On "rempli une case" = on supprime une case "libre"
    sem_wait(&semaphoreTailleRestante);
    sem_wait(&semaphoreAccesUnique);

    assert(bag-> is_closed == 0); // adding to a closed bag is an error
    assert(bag->count < bag->size); // sanity check

    //printf("Entrée section critique (bag add)\n");
    bag->elem[bag->count] = element;
    bag->count += 1;

    //printf("Sortie section critique (bag add)\n");
    sem_post(&semaphoreAccesUnique);
    //On "rempli une case" = on note une case de plus comme "utilisé"
    sem_post(&semaphoreTailleUsed);

}

void * bb_take(bag_t *bag) {
    assert(bag != NULL); // sanity check

    //while( bag->count <= 0 ) { } // CAUTION: this synchronization is bogus
    //On "vide une case" = on supprime une case "utilisé"
    sem_wait(&semaphoreTailleUsed);
    sem_wait(&semaphoreAccesUnique);
    //    assert(bag->count > 0); // sanity check
    
    //Si il est fermé, on sort.
    if (bag->is_closed && bag->count == 0) {
        //On rend le jeton (on ne part pas avec !)
        sem_post(&semaphoreAccesUnique);
        return NULL;
    }
    
    bag->count -= 1;
    void *r = bag->elem[bag->count];

    sem_post(&semaphoreAccesUnique);
    //On "vide une case" = on ajoute une case "libre"
    sem_post(&semaphoreTailleRestante);

    //usleep(10);// artificial delay to increase the occurence of race conditions
    return r;
}

void bb_close(bag_t *bag, int N) {
    //assert("not implemented" == 0);

    //Si le bag n'est pas déjà fermé.
    if (!bag->is_closed) {
        printf("Fermeture du sac lancée : %d éléments restants.\n", bag->count);
        //On devra débloquer le bag. On supprime donc les attributs du bag.
        bag->is_closed = 1;

        //On débloque les thread consommateurs en augmentant les cases "utilisées" = on fait croire qu'il y a des éléments à récupérer.
        for (int tnum = 0; tnum < N; tnum++) {
            sem_post(&semaphoreTailleUsed);
        }
    }

    // sem_destroy() ?
}
